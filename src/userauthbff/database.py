""" database.py """


from databases import Database
import sqlalchemy

import configuration.configparser


class Connection:
    """ Connection """

    def __init__(self, app_config: configuration.configparser.Config) -> None:
        """ __init__ """
        self.config = app_config
        username = app_config.get_value("BFF_DB_USER")
        password = app_config.get_value("BFF_DB_PASSWORD")
        host = app_config.get_value("BFF_DB_HOST")
        port = app_config.get_value("BFF_DB_PORT")
        db_name = app_config.get_value("BFF_DB_NAME")
        self.connection = Database(
            f"postgresql://{username}:{password}@{host}:{port}/{db_name}"
        )


class DbContext:
    """ DbContext """

    def __init__(self, connection: Connection,) -> None:
        """ __init__ """
        self.connection = connection
        self.metadata = sqlalchemy.MetaData()
        self.user_info = sqlalchemy.Table(
            "user_info",
            self.metadata,
            sqlalchemy.Column(
                "user_id", sqlalchemy.Text, primary_key=True, nullable=False
            ),
            sqlalchemy.Column("user_data", sqlalchemy.JSON, nullable=False),
            sqlalchemy.UniqueConstraint("user_id", name="unique_user_id_in_user_id"),
        )
