""" config.py """


import configuration.configparser


APP_CONFIG: configuration.configparser.Config = configuration.configparser.ConfigParser.load()
