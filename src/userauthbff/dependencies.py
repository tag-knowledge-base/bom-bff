""" dependencies.py """


import pinject

from . import database, config


class AppBindingSpec(pinject.BindingSpec):  # type: ignore
    """ AppBindingSpec """

    def configure(self, bind) -> None:  # type: ignore
        """ configure """
        bind("app_config", to_instance=config.APP_CONFIG)
        bind("db_context", to_class=database.DbContext, in_scope=pinject.PROTOTYPE)


APP_GRAPH = pinject.new_object_graph(
    modules=[
        config,
        database,
    ],
    binding_specs=[AppBindingSpec()],
)
