""" exceptions.py """


class AiServiceException(Exception):
    """ AiServiceException """

    def __init__(
        self, localization_key: str, message: str, status_code: int,
    ):
        """ __init__ """
        super().__init__()
        self.localization_key = localization_key
        self.message = message
        self.status_code = status_code


class InternalServerError(AiServiceException):
    """ InternalServerError """

    def __init__(self,):
        """ __init__ """
        super().__init__("userauthbff-servererror", "Internal Server Error", 500)


class CannotVerifyGmailUser(AiServiceException):
    """ CannotVerifyGmailUser """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthbff-cannotverifygmailuser", "Cannot verify gmail user", 404
        )


class UserIdCollision(AiServiceException):
    """ UserIdCollision """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthbff-useridcollision", "User Id exists in database already", 409
        )


class UserNotFound(AiServiceException):
    """ UserNotFound """

    def __init__(self,):
        """ __init__ """
        super().__init__("userauthbff-usernotfound", "User not found.", 404)


class CouldNotRegisterUser(AiServiceException):
    """ CouldNotRegisterUser """

    def __init__(self,):
        """ __init__ """
        super().__init__("userauthbff-couldnotregisteruser", "Could not register user.", 400)


class UserHasAlreadyRegistered(AiServiceException):
    """ UserHasAlreadyRegistered """

    def __init__(self,):
        """ __init__ """
        super().__init__("userauthbff-userhasalreadyregistered", "User has already registered.", 409)


class TokenDoesNotBelongToUserId(AiServiceException):
    """ TokenDoesNotBelongToUserId """

    def __init__(self,):
        """ __init__ """
        super().__init__("userauthbff-tokendoesnotbelongtouserid", "Token does not belong to user.", 404)


class NoUserIdHeader(AiServiceException):
    """ NoUserIdHeader """

    def __init__(self,):
        """ __init__ """
        super().__init__("userauthbff-nouseridheader", "No user id header provided", 400)
