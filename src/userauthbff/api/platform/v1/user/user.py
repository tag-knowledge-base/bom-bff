""" user.py """


import datetime
import uuid

import pytz

from . import dtos


class User:
    """ User """

    def __init__(self) -> None:
        """ __init__ """
        _now = datetime.datetime.now(pytz.UTC)
        self.created_on = _now
        self.last_modified_on = _now
        self.user_status = dtos.StatusType.active
        self.role = dtos.RoleType.user
        self.identifier = uuid.uuid4()
