""" controller.py """


import hashlib
import logging
from typing import Dict, Any

from asyncpg.exceptions import UniqueViolationError
from fastapi import APIRouter, Depends, Header
from google.oauth2 import id_token
from google.auth.transport import requests
import httpx
import jsonpickle

from ..... import config, database, exceptions
from ..... import dependencies as app_dependencies
from .. import authentication
from . import dtos, user


_logger = logging.getLogger(__name__)


ROUTER = APIRouter()


@ROUTER.post("/gmail", response_model=dtos.UserDto, tags=["user"])
async def async_gmail_create_user(
    x_user_id: str = Depends(authentication.async_get_user_hash),
) -> dtos.UserDto:
    """ async_gmail_create_user """
    _user = user.User()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        query = db_context.user_info.insert()
        values = {
            "user_id": x_user_id,
            "user_data": jsonpickle.encode(_user),
        }
        try:
            await db_context.connection.connection.execute(query=query, values=values)
        except UniqueViolationError:
            raise exceptions.UserIdCollision()
    return dtos.UserDto(
        user_id=x_user_id,
        user_data=dtos.UserInDbDto(
            created_on=_user.created_on,
            last_modified_on=_user.last_modified_on,
            user_status=_user.user_status,
            role=_user.role,
            identifier=_user.identifier,
        ),
    )


@ROUTER.get("/gmail", response_model=dtos.UserDto, tags=["user"])
async def async_get_user(
    x_user_id: str = Depends(authentication.async_get_user_hash),
) -> dtos.UserDto:
    """ async_get_user """
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        query = db_context.user_info.select().where(
            db_context.user_info.c.user_id == x_user_id
        )
        user_found = await db_context.connection.connection.fetch_one(query=query)
        if not user_found:
            raise exceptions.UserNotFound()
        user_decoded = jsonpickle.decode(user_found.get("user_data"))
        return dtos.UserDto(
            user_id=x_user_id,
            user_data=dtos.UserInDbDto(
                created_on=user_decoded.created_on,
                last_modified_on=user_decoded.last_modified_on,
                user_status=user_decoded.user_status,
                role=user_decoded.role,
                identifier=user_decoded.identifier,
            ),
        )


@ROUTER.post("/register/gmail", response_model=str, tags=["user"])
async def async_register_gmail_user(
    x_user_id: str = Depends(authentication.async_get_user_hash),
) -> str:
    """ async_register_gmail_user """
    async with httpx.AsyncClient() as client:
        res = await client.post(
            config.APP_CONFIG.get_value("USER_AUTH_URL") + "/api/platform/v1/user",
            headers={"user-id": x_user_id},
        )
        if res.status_code == 200:
            return str(res.json())
        if res.status_code == 400:
            raise exceptions.CouldNotRegisterUser()
        if res.status_code == 409:
            raise exceptions.UserHasAlreadyRegistered()
        raise exceptions.InternalServerError()


@ROUTER.get("/register/gmail", response_model=dtos.TagUserDto, tags=["user"])
async def async_get_tag_user(
    x_user_id: str = Depends(authentication.async_get_user_hash),
) -> dtos.TagUserDto:
    """ async_get_tag_user """
    async with httpx.AsyncClient() as client:
        res = await client.get(
            config.APP_CONFIG.get_value("USER_AUTH_URL") + "/api/platform/v1/user",
            headers={"user-id": x_user_id},
        )
        if res.status_code == 200:
            return dtos.TagUserDto(**res.json())
        if res.status_code in (400, 404):
            raise exceptions.UserNotFound()
        raise exceptions.InternalServerError()


@ROUTER.delete("/register/gmail", response_model=str, tags=["user"])
async def async_delete_registered_user(
    x_user_id: str = Depends(authentication.async_get_user_hash),
) -> str:
    """ async_delete_registered_user """
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with httpx.AsyncClient() as client:
        res = await client.delete(
            config.APP_CONFIG.get_value("USER_AUTH_URL") + "/api/platform/v1/user",
            headers={"user-id": x_user_id},
        )
        if res.status_code != 200:
            raise exceptions.InternalServerError()
    async with db_context.connection.connection.connection():
        query = db_context.user_info.delete().where(
            db_context.user_info.c.user_id == x_user_id
        )
        await db_context.connection.connection.execute(query=query)


@ROUTER.post("/song/like", response_model=dtos.LikeSongDto, tags=["user"])
async def async_like_song(like_song_dto: dtos.LikeSongDto, x_user_id: str = Depends(authentication.async_get_user_hash)) -> dtos.LikeSongDto:
    """ async_like_song """
    async with httpx.AsyncClient() as client:
        res = await client.post(
            config.APP_CONFIG.get_value("USER_AUTH_URL") + "/api/platform/v1/user/song/like",
            json=like_song_dto.dict(),
            headers={"user-id": x_user_id},
        )
        if res.status_code != 200:
            raise exceptions.InternalServerError()
        return like_song_dto
