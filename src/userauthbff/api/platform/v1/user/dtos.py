""" dtos.py """


import datetime
from enum import Enum
from typing import Dict, Any, Union, List
import uuid

import pydantic


class AttitudeType(str, Enum):
    """ AttitudeType """

    indifferent = "Indifferent"
    like = "Like"
    dislike = "Dislike"


class RoleType(str, Enum):
    """ RoleType """

    user = "User"
    admin = "Admin"


class StatusType(str, Enum):
    """ StatusType """

    undefined = "Undefined"
    active = "Active"
    inactive = "Inactive"


class ContributionAction(str, Enum):
    """ ContributionAction """

    undefined = "Undefined"
    created = "Created"
    deleted = "Deleted"


class ContributionType(str, Enum):
    """ ContributionType """

    undefined = "Undefined"
    tag = "Tag"
    song = "Song"


class ContributionDto(pydantic.BaseModel):
    """ ContributionDto """

    aggregate_id: str
    contributed_on: datetime.datetime
    contribution_type: ContributionType = ContributionType.undefined
    action_type: ContributionAction = ContributionAction.undefined


class CreateGmailUserDto(pydantic.BaseModel):
    """ UserDto """

    gmail_user_token: str


class UserInDbDto(pydantic.BaseModel):
    """ UserInDbDto """

    created_on: datetime.datetime
    last_modified_on: datetime.datetime
    user_status: StatusType
    role: RoleType
    identifier: uuid.UUID


class UserDto(pydantic.BaseModel):
    """ UserDto """

    user_id: str
    user_data: UserInDbDto


class MetaData(pydantic.BaseModel):
    """ MetaData """

    created_by: str
    created_on: datetime.datetime
    last_modified_by: str
    last_modified_on: datetime.datetime


class SongAttitude(pydantic.BaseModel):
    """ SongAttitude """

    song: str
    attitude: AttitudeType
    created_on: datetime.datetime


class TagUserDto(pydantic.BaseModel):
    """ TagUserDto """

    aggregate_id: str
    metadata: Union[MetaData, None]
    contributions: List[ContributionDto]
    liked_songs: List[SongAttitude] = []


class LikeSongDto(pydantic.BaseModel):
    """ LikeSongDto """

    song_aggregate_id: str
    attitude_type: AttitudeType
