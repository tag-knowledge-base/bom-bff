""" controller.py """


import hashlib
import logging
from typing import Dict, Any, List

from asyncpg.exceptions import UniqueViolationError
from fastapi import APIRouter, Depends, Header
from google.oauth2 import id_token
from google.auth.transport import requests
import httpx
import jsonpickle

from ..... import config, database, exceptions
from ..... import dependencies as app_dependencies
from .. import authentication
from . import dtos, song


_logger = logging.getLogger(__name__)


ROUTER = APIRouter()


@ROUTER.get("", response_model=List[dtos.SongDetailDto], tags=["song"])
async def async_get_all_songs(
    x_user_id: str = Depends(authentication.async_get_user_hash),
) -> List[dtos.SongDetailDto]:
    """ async_get_tag_user """
    async with httpx.AsyncClient() as client:
        res = await client.get(
            config.APP_CONFIG.get_value("USER_AUTH_URL") + "/api/platform/v1/song",
            headers={"user-id": x_user_id},
        )
        if res.status_code == 200:
            return [dtos.SongDetailDto(**i) for i in res.json()]
        raise exceptions.InternalServerError()


@ROUTER.post("", response_model=dtos.CreateSongDto, tags=["song"])
async def async_create_song(
    create_song: dtos.CreateSongDto,
    x_user_id: str = Depends(authentication.async_get_user_hash)
) -> dtos.CreateSongDto:
    """ async_create_song """
    async with httpx.AsyncClient() as client:
        res = await client.post(
            config.APP_CONFIG.get_value("USER_AUTH_URL") + "/api/platform/v1/song",
            json=create_song.dict(),
            headers={"user-id": x_user_id},
        )
        if res.status_code == 200:
            return dtos.CreateSongDto(**res.json())
        raise exceptions.InternalServerError()


@ROUTER.post("/clear/bulk", response_model=List[str], tags=["song"])
async def async_bulk_delete_songs(
    delete_song: List[str],
    x_user_id: str = Depends(authentication.async_get_user_hash)
) -> List[str]:
    """ async_delete_songs """
    async with httpx.AsyncClient() as client:
        for _song in delete_song:
            await client.delete(
                config.APP_CONFIG.get_value("USER_AUTH_URL") + f"/api/platform/v1/song/{_song}",
                headers={"user-id": x_user_id},
            )
        return delete_song
