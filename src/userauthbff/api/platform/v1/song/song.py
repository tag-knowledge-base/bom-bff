""" song.py """


import datetime
from typing import List


class MetaData:
    """ MetaData """

    def __init__(self, created_by: str, created_on: datetime.datetime) -> None:
        """ __init__ """
        self.created_by = created_by
        self.created_on = created_on
        self.last_modified_by = self.created_by
        self.last_modified_on = self.created_on

    def update(self, updated_by: str, updated_on: datetime.datetime) -> None:
        """ update """
        self.last_modified_on = updated_on
        self.last_modified_by = updated_by


class Song:
    """ Song """

    def __init__(
        self,
        aggregate_id: str,
        song: str,
        artist: List[str],
        album: str,
        created_by: str,
        created_on: datetime.datetime,
        track_position: int = 0,
    ) -> None:
        """ __init__ """
        self.aggregate_id = aggregate_id
        self.song = song
        self.artist = artist
        self.album = album
        self.track_position = track_position
        self.metadata = MetaData(created_by, created_on)
