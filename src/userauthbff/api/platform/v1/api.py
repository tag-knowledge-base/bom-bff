""" api.py """


from fastapi import APIRouter

from .song import controller as song_controller
from .user import controller as user_controller


V1_ROUTER = APIRouter()
V1_ROUTER.include_router(user_controller.ROUTER, prefix="/user")
V1_ROUTER.include_router(song_controller.ROUTER, prefix="/song")
