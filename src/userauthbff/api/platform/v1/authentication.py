""" authentication.py """


import hashlib
import logging
from typing import Dict, Any

from fastapi import Header, Depends
from google.oauth2 import id_token
from google.auth.transport import requests

from .... import config, exceptions


MAY_2020_SALT = config.APP_CONFIG.get_value("MAY_2020_SALT")


_logger = logging.getLogger(__name__)


async def async_authenticate_token(x_gmail_token: str = Header(None)) -> Dict[str, Any]:
    """ async_authenticate_token """
    app_config = config.APP_CONFIG
    try:
        id_info = id_token.verify_oauth2_token(
            x_gmail_token, requests.Request(), app_config.get_value("CLIENT_ID")
        )
        if id_info["iss"] not in ["accounts.google.com", "https://accounts.google.com"]:
            raise ValueError("Token not issued from google.")
        if not id_info["email_verified"]:
            raise ValueError("Email not verified")
    except ValueError:
        raise exceptions.CannotVerifyGmailUser()
    return id_info


async def async_generate_user_hash(user_id: str) -> str:
    """ async_generate_user_hash """
    return hashlib.sha256(
        (user_id + MAY_2020_SALT).encode("UTF-8")
    ).hexdigest()


async def async_get_user_hash(id_info: Dict[str, Any] = Depends(async_authenticate_token)) -> str:
    """ async_get_user_hash """
    return await async_generate_user_hash(id_info["email"])
