""" settings.py """


import os
from pydantic import BaseModel  # pylint: disable=E0401


class Configuration(BaseModel):  # pylint: disable=too-few-public-methods
    """ configuration """

    APP_NAME = os.getenv("APP_NAME", "user-auth-bff")
    APP_VERSION = os.getenv("APP_VERSION", "1.0.0-rc.4")
    BFF_DB_USER = os.getenv("BFF_DB_USER", "bff_user")
    BFF_DB_PASSWORD = os.getenv("BFF_DB_PASSWORD", "bff_user")
    BFF_DB_NAME = os.getenv("BFF_DB_NAME", "userauthbff")
    BFF_DB_HOST = os.getenv("BFF_DB_HOST", "localhost")
    BFF_DB_PORT = os.getenv("BFF_DB_PORT", "5432")
    BFF_ALEMBIC_SCRIPT_LOCATION = os.getenv(
        "BFF_ALEMBIC_SCRIPT_LOCATION", "userauthbff"
    )
    USER_AUTH_URL = os.getenv("USER_AUTH_URL", "http://localhost:8001")
    CLIENT_ID = os.getenv("CLIENT_ID")
    MAY_2020_SALT = os.getenv("MAY_2020_SALT")


CONFIGURATION = Configuration()
