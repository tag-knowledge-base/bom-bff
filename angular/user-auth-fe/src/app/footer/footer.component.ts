import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-footer",
  template: `
    <hr width="80%" />
    <div class="copyright">ONTOSOUND</div>
  `,
  styles: [
    `
      .mat-icon-button.footer {
        height: 20px;
        width: 20px;
        margin: 10px;
      }
    `,
    `
      .copyright {
        opacity: 0.7;
        font-size: 10%;
        display: flex;
        justify-content: center;
      }
    `,
    `
      .toolbar {
        margin-top: 10%;
        text-align: center;
      }
    `
  ]
})
export class FooterComponent implements OnInit {
  public github_url: string;
  public linkedin_url: string;
  public gmail_name: string;
  constructor() {}

  ngOnInit() {
    this.github_url = "data.contact.github";
    this.linkedin_url = "data.contact.linkedin";
    this.gmail_name = "data.contact.gmail";
  }
}
