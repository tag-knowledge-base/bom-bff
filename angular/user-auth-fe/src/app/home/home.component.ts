import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  template: `
    <mat-grid-list cols="4" rowHeight="100px">
      <mat-grid-tile [colspan]="4" [rowspan]="2" [style.background]="'#EDE7F6'">
        <div class="home-page-paragraph-text">
          <h1>About</h1>
          <p>
            ONTOSOUND is a community driven platform for users to explore music
            listening habits through data insights and machine learning
            technologies. As a user, you can freely contribute through the UI or
            through the APIs available. You can delete your account at any time,
            but note that your data is available for users. The only personal
            data stored related to your email account is a hashed version of
            your email. Data obtained while using the website is stored and
            available for users of the community.
          </p>
        </div>
      </mat-grid-tile>
      <mat-grid-tile [colspan]="4" [rowspan]="2" [style.background]="'#B39DDB'">
        <div class="home-page-paragraph-text">
          <h1>Contact</h1>
          <div>
            Email: administrator@ontosound.com
          </div>
          <div>
            Instagram: @ontosoundcom
          </div>
        </div>
      </mat-grid-tile>
    </mat-grid-list>
  `,
  styles: [
    `
      .example-fill-remaining-space {
        flex: 1 1 auto;
      }
    `,
    `
      .home-page-paragraph-text {
        padding-top: 50px;
        padding-right: 30px;
        padding-bottom: 50px;
        padding-left: 80px;
        text-align: center;
      }
    `
  ]
})
export class HomeComponent implements OnInit {
  constructor() {}
  ngOnInit(): void {}
}
