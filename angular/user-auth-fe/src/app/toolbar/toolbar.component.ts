import { Component, OnInit, Output } from "@angular/core";
import { TranslationsService } from "../_services/translations.service";
import { EventEmitter } from "@angular/core";
import { AuthenticationService } from "../_services/authentication.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-toolbar",
  template: `
    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <span>{{ appName }}</span>
        <span class="example-fill-remaining-space"></span>
        <ng-template [ngIf]="loggedIn">
          <span>
            <button mat-button routerLink="/contribute" color="secondary">
              Contribute
            </button>
          </span>
          <span>
            <button mat-button routerLink="/profile" color="secondary">
              Profile
            </button>
          </span>
          <span>
            <button mat-button routerLink="/home" color="secondary">
              Home
            </button>
          </span>
        </ng-template>
        <span><app-logout></app-logout></span>
      </mat-toolbar-row>
    </mat-toolbar>
  `,
  styles: [
    `
      .example-fill-remaining-space {
        flex: 1 1 auto;
      }
    `
  ]
})
export class ToolbarComponent implements OnInit {
  public appName: string = "ONTOSOUND";
  public loggedIn: boolean = false;
  public logoutValue: string = "Login";

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.authenticationService.currentUserSubject.subscribe(val => {
      if (val) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });
  }

  async logoutUser() {
    if (!this.authenticationService.currentUserValue) {
      await this.authenticationService.login();
      if (!this.authenticationService.currentUserValue) {
        await this.authenticationService.logout();
        this.logoutValue = "Login";
        this.router.navigate([""]);
      } else {
        this.logoutValue = "Logout";
        this.router.navigate(["/home"]);
      }
    } else {
      await this.authenticationService.logout();
      this.logoutValue = "Login";
      this.router.navigate([""]);
    }
  }
}
