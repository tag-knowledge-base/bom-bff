import { AttitudeType } from "./status";

export interface SongDto {
  song: string;
  artist: string[];
  album: string;
  track_position: number;
  aggregate_id: string;
}

export interface CreateSongDto {
  song: string;
  artist: string[];
  album: string;
  track_position: number;
}

export interface LikeSongDto {
  song_aggregate_id: string;
  attitude_type: AttitudeType;
}
