import { Role } from "./role";
import { Status, AttitudeType } from "./status";

export interface CreateUser {
  user_token: string;
}

export interface UserInDb {
  created_on: Date;
  last_modified_on: Date;
  user_status: Status;
  role: Role;
  identifier: string;
}

export interface User {
  user_id: string;
  user_data: UserInDb;
  user_token?: string;
  user_name?: string;
}

export interface MetaData {
  created_by: string;
  created_on: Date;
  last_modified_by: string;
  last_modified_on: Date;
}

export enum ContributionAction {
  Undefined = "Undefined",
  Created = "Created",
  Edited = "Edited",
  Cleared = "Cleared"
}

export enum ContributionType {
  Undefined = "Undefined",
  Tag = "Tag",
  Song = "Song"
}

export interface Contribution {
  aggregate_id: string;
  contributed_on: Date;
  contribution_type: ContributionType;
  action_type: ContributionAction;
}

export interface SongAttitude {
  song: string;
  attitude: AttitudeType;
  created_on: Date;
}

export interface TagUser {
  aggregate_id: string;
  metadata?: MetaData;
  contributions: Contribution[];
  liked_songs: SongAttitude[];
}

export interface UserView {
  created_on: Date;
  last_modified_on: Date;
  user_status: Status;
  role: Role;
  name: string;
  registered: boolean;
}
