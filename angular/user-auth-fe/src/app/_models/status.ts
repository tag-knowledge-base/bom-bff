export enum Status {
  Undefined = "Undefined",
  Active = "Active",
  Inactive = "Inactive"
}

export enum AttitudeType {
  Indifferent = "Indifferent",
  Like = "Like",
  Dislike = "Dislike"
}
