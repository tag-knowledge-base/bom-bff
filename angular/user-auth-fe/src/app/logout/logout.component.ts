import { Component, OnInit } from "@angular/core";
import { TranslationsService } from "../_services/translations.service";
import { AuthenticationService } from "../_services/authentication.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-logout",
  template: `
    <button mat-button (click)="logoutUser()" color="secondary">
      {{ logout }}
    </button>
  `,
  styles: [
    `
      .example-fill-remaining-space {
        flex: 1 1 auto;
      }
    `
  ]
})
export class LogoutComponent implements OnInit {
  public logout: string = "Logout";

  constructor(
    private translationService: TranslationsService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.authenticationService.currentUserSubject.subscribe(val =>
      this.translationService.languageSelected.subscribe(lang => {
        this.logout = this.translationService.translations.logout.logout[lang];
        if (!val) {
          this.logout = this.translationService.translations.logout.login[lang];
        } else {
          this.logout = this.translationService.translations.logout.logout[
            lang
          ];
        }
      })
    );
  }

  async logoutUser() {
    if (!this.authenticationService.currentUserValue) {
      await this.authenticationService.login();
      if (!this.authenticationService.currentUserValue) {
        await this.authenticationService.logout();
        this.translationService.languageSelected.subscribe(lang => {
          this.logout = this.translationService.translations.logout.login[lang];
        });
        this.router.navigate([""]);
      } else {
        this.translationService.languageSelected.subscribe(lang => {
          this.logout = this.translationService.translations.logout.logout[
            lang
          ];
        });
        this.router.navigate(["/home"]);
      }
    } else {
      await this.authenticationService.logout();
      this.translationService.languageSelected.subscribe(lang => {
        this.logout = this.translationService.translations.logout.login[lang];
      });
      this.router.navigate([""]);
    }
  }
}
