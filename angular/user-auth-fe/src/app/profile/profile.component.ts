import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../_services/authentication.service";
import {
  TagUser,
  UserInDb,
  User,
  Contribution,
  ContributionType,
  ContributionAction
} from "../_models/user";
import { Role } from "../_models/role";
import { Status, AttitudeType } from "../_models/status";
import { Router } from "@angular/router";
import { Action } from "rxjs/internal/scheduler/Action";

export interface UiContribution {
  aggregate_id: string;
  action_type: ContributionAction;
  contribution_type: ContributionType;
  contributed_on: string;
}

export interface UiLikedSongs {
  song: string;
  attitude: AttitudeType;
  date: string;
}

@Component({
  selector: "app-profile",
  template: `
    <mat-tab-group>
      <mat-tab label="Basic">
        <div class="center-div">
          <mat-card class="center-card">
            <mat-card-header class="center-text">
              <mat-card-title>{{ name }}</mat-card-title>
            </mat-card-header>
            <mat-card-content>
              <p style="text-align:left">
                Role
                <span style="float:right;">
                  {{ role }}
                </span>
              </p>
              <p style="text-align:left">
                Status
                <span style="float:right;">
                  {{ status }}
                </span>
              </p>
              <p style="text-align:left">
                Created On
                <span style="float:right;">
                  {{ createdOn }}
                </span>
              </p>
              <p style="text-align:left">
                Last Modified On
                <span style="float:right;">
                  {{ lastModifiedOn }}
                </span>
              </p>
            </mat-card-content>
            <button mat-raised-button color="primary" (click)="onDelete()">
              Delete Account
            </button>
          </mat-card>
        </div>
      </mat-tab>
      <mat-tab label="Contributions">
        <div *ngFor="let contribution of contributions">
          <mat-card>
            <mat-card-content>
              <p style="text-align:left">
                Identifier
                <span style="float:right;">
                  {{ contribution.aggregate_id }}
                </span>
              </p>
              <p style="text-align:left">
                Type
                <span style="float:right;">
                  {{ contribution.contribution_type }}
                </span>
              </p>
              <p style="text-align:left">
                Action
                <span style="float:right;">
                  {{ contribution.action_type }}
                </span>
              </p>
              <p style="text-align:left">
                Date
                <span style="float:right;">
                  {{ contribution.contributed_on }}
                </span>
              </p>
            </mat-card-content>
          </mat-card>
        </div>
      </mat-tab>
      <mat-tab label="Liked Songs">
        <div *ngFor="let liked_song of liked_songs">
          <mat-card>
            <mat-card-content>
              <p style="text-align:left">
                Identifier
                <span style="float:right;">
                  {{ liked_song.song }}
                </span>
              </p>
              <p style="text-align:left">
                Attitude
                <span style="float:right;">
                  {{ liked_song.attitude }}
                </span>
              </p>
              <p style="text-align:left">
                Date
                <span style="float:right;">
                  {{ liked_song.date }}
                </span>
              </p>
            </mat-card-content>
          </mat-card>
        </div>
      </mat-tab>
    </mat-tab-group>
  `,
  styles: [
    `
      .center-div {
        justify-content: center;
        margin-top: 1%;
        margin-bottom: 1%;
      }
    `,
    `
      .center-card {
        justify-content: center;
      }
    `,
    `
      .example-fill-remaining-space {
        flex: 1 1 auto;
      }
    `,
    `
      .center-text {
        justify-content: center;
      }
    `
  ]
})
export class ProfileComponent implements OnInit {
  public name?: string;
  public role?: Role;
  public status?: Status;
  public createdOn?: string;
  public lastModifiedOn?: string;
  public contributions: UiContribution[] = [];
  public liked_songs: UiLikedSongs[] = [];

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.authenticationService.userAggregate().then(val => {
      if (val) {
        this.createdOn = new Date(val.metadata.created_on).toLocaleString();
        this.lastModifiedOn = new Date(
          val.metadata.last_modified_on
        ).toLocaleString();
        if (val.contributions) {
          val.contributions.forEach(val => {
            this.contributions.push({
              contributed_on: new Date(val.contributed_on).toLocaleString(),
              contribution_type: val.contribution_type,
              action_type: val.action_type,
              aggregate_id: val.aggregate_id
            });
          });
        }
        console.log(val.liked_songs);
        if (val.liked_songs) {
          val.liked_songs.forEach(val => {
            this.liked_songs.push({
              song: val.song,
              attitude: val.attitude,
              date: new Date(val.created_on).toLocaleString()
            });
          });
        }
      }
    });
    this.authenticationService.currentUserSubject.subscribe(val => {
      if (val) {
        this.name = val.user_name;
        this.role = val.user_data.role;
        this.status = val.user_data.user_status;
      }
    });
  }

  ngOnInit(): void {}

  async onDelete() {
    await this.authenticationService.deleteUser();
    await this.authenticationService.logout();
    this.router.navigate(["/"]);
  }
}
