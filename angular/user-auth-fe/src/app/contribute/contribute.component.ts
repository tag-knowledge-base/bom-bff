import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { Component, OnInit, Inject } from "@angular/core";
import { SongDto } from "../_models/tag";
import { AuthenticationService } from "../_services/authentication.service";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
  MatChipInputEvent
} from "@angular/material";
import { FormControl } from "@angular/forms";
import { BehaviorSubject } from "rxjs";
import { SelectionModel } from "@angular/cdk/collections";
import { AttitudeType } from "../_models/status";

@Component({
  selector: "app-contribute",
  template: `
    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <span class="example-fill-remaining-space"></span>
        <span>
          <button mat-button (click)="rateSongs()" color="secondary">
            Rate Songs
          </button>
        </span>
        <span>
          <button mat-button (click)="deleteSongs()" color="secondary">
            Delete Songs
          </button>
        </span>
        <span>
          <button mat-button (click)="addSong()" color="secondary">
            Add Song
          </button>
        </span>
      </mat-toolbar-row>
    </mat-toolbar>
    <div *ngIf="dataSource.length > 0">
      <mat-table [dataSource]="dataSource" class="mat-elevation-z8">
        <ng-container matColumnDef="select">
          <mat-header-cell *matHeaderCellDef>
            <mat-checkbox
              (change)="$event ? masterToggle() : null"
              [checked]="selection.hasValue() && isAllSelected()"
              [indeterminate]="selection.hasValue() && !isAllSelected()"
              [aria-label]="checkboxLabel()"
            >
            </mat-checkbox>
          </mat-header-cell>
          <mat-cell *matCellDef="let row">
            <mat-checkbox
              (click)="$event.stopPropagation()"
              (change)="$event ? selection.toggle(row) : null"
              [checked]="selection.isSelected(row)"
              [aria-label]="checkboxLabel(row)"
            >
            </mat-checkbox>
          </mat-cell>
        </ng-container>
        <ng-container matColumnDef="Song">
          <mat-header-cell *matHeaderCellDef> Song </mat-header-cell>
          <mat-cell *matCellDef="let element"> {{ element.song }} </mat-cell>
        </ng-container>
        <ng-container matColumnDef="Artist">
          <mat-header-cell *matHeaderCellDef> Artist </mat-header-cell>
          <mat-cell *matCellDef="let element">
            {{ element.artist.join(", ") }}
          </mat-cell>
        </ng-container>
        <ng-container matColumnDef="Album">
          <mat-header-cell *matHeaderCellDef> Album </mat-header-cell>
          <mat-cell *matCellDef="let element"> {{ element.album }} </mat-cell>
        </ng-container>
        <ng-container matColumnDef="Track Number">
          <mat-header-cell *matHeaderCellDef> Track Number </mat-header-cell>
          <mat-cell *matCellDef="let element">
            {{ element.track_position }}
          </mat-cell>
        </ng-container>
        <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
        <mat-row *matRowDef="let row; columns: displayedColumns"></mat-row>
      </mat-table>
    </div>
  `,
  styles: [
    `
      .example-fill-remaining-space {
        flex: 1 1 auto;
      }
    `
  ]
})
export class ContributeComponent implements OnInit {
  public dataSource: SongDto[] = [];
  public displayedColumns: string[] = [
    "select",
    "Song",
    "Artist",
    "Album",
    "Track Number"
  ];
  selection = new SelectionModel<SongDto>(true, []);

  constructor(
    public dialog: MatDialog,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.authenticationService.getSongs().then(val => {
      this.dataSource = val;
    });
  }

  addSong(): void {
    this.dialog
      .open(ContributeDialogComponent)
      .afterClosed()
      .subscribe(val => {
        this.selection.clear();
        this.authenticationService.getSongs().then(val => {
          this.dataSource = val;
        });
      });
  }

  async rateSongs() {
    this.dialog
      .open(RateDialogComponent, {
        data: this.selection.selected.map(sel => sel.aggregate_id)
      })
      .afterClosed()
      .subscribe(val => {
        this.selection.clear();
        this.authenticationService.getSongs().then(val => {
          this.dataSource = val;
        });
      });
  }

  async deleteSongs() {
    await this.authenticationService
      .deleteSongsBulk(this.selection.selected.map(sel => sel.aggregate_id))
      .finally(() => {
        this.selection.clear();
        this.authenticationService.getSongs().then(val => {
          this.dataSource = val;
        });
      });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: SongDto): string {
    if (!row) {
      return `${this.isAllSelected() ? "select" : "deselect"} all`;
    }
    return `${this.selection.isSelected(row) ? "deselect" : "select"} row ${
      row.aggregate_id
    }`;
  }
}

export interface Artist {
  name: string;
}

@Component({
  selector: "app-contribute-dialog",
  template: `
    <p>
      <mat-form-field>
        <mat-label>Song Name</mat-label>
        <input matInput [(ngModel)]="song" />
      </mat-form-field>
    </p>
    <p>
      <mat-form-field>
        <mat-chip-list #chipList aria-label="Artist selection">
          <mat-chip
            *ngFor="let artist of artists"
            [selectable]="selectable"
            [removable]="removable"
            (removed)="remove(artist)"
          >
            {{ artist.name }}
            <mat-icon matChipRemove *ngIf="removable">cancel</mat-icon>
          </mat-chip>
          <input
            placeholder="Artist..."
            [matChipInputFor]="chipList"
            [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
            [matChipInputAddOnBlur]="addOnBlur"
            (matChipInputTokenEnd)="add($event)"
          />
        </mat-chip-list>
      </mat-form-field>
    </p>
    <p>
      <mat-form-field>
        <mat-label>Album</mat-label>
        <input matInput [(ngModel)]="album" />
      </mat-form-field>
    </p>
    <p>
      <mat-form-field>
        <mat-label>Track Position</mat-label>
        <input matInput [(ngModel)]="track_position" />
      </mat-form-field>
    </p>
    <button mat-raised-button color="primary" (click)="createSong()">
      Create
    </button>
  `,
  styles: [``]
})
export class ContributeDialogComponent {
  public song: string;
  public artists: Artist[] = [];
  public album: string;
  public track_position: number;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [COMMA, ENTER];

  constructor(
    private authenticationService: AuthenticationService,
    public dialogRef: MatDialogRef<ContributeDialogComponent>
  ) {}

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || "").trim()) {
      let found: boolean = false;
      this.artists.forEach(element => {
        if (element.name == value.trim()) {
          found = true;
        }
      });
      if (!found) {
        this.artists.push({ name: value.trim() });
      }
    }
    if (input) {
      input.value = "";
    }
  }

  remove(artist: Artist): void {
    const index = this.artists.indexOf(artist);
    if (index >= 0) {
      this.artists.splice(index, 1);
    }
  }

  async createSong() {
    await this.authenticationService.createSong({
      song: this.song,
      artist: this.artists.map(val => val.name),
      album: this.album,
      track_position: this.track_position
    });
  }
}

@Component({
  selector: "app-rate-dialog",
  template: `
    <label id="rate-songs-label">Rate Songs</label>
    <mat-radio-group
      class="attitude-radio-group"
      aria-labelledby="rate-songs-label"
      [(ngModel)]="attitude"
    >
      <mat-radio-button
        class="attitude-radio-button"
        *ngFor="let rating of attitudes"
        [value]="rating"
      >
        {{ rating }}
      </mat-radio-button>
    </mat-radio-group>
    <button mat-raised-button color="primary" (click)="rateSongs()">
      Rate
    </button>
  `,
  styles: [
    `
      .attitude-radio-group {
        display: flex;
        flex-direction: column;
        margin: 15px 0;
      }
    `,
    `
      .attitude-radio-button {
        margin: 5px;
      }
    `
  ]
})
export class RateDialogComponent {
  attitude: AttitudeType;
  attitudes: AttitudeType[] = [
    AttitudeType.Indifferent,
    AttitudeType.Like,
    AttitudeType.Dislike
  ];
  constructor(
    private authenticationService: AuthenticationService,
    public dialogRef: MatDialogRef<ContributeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string[]
  ) {}

  async rateSongs() {
    console.log(this.data);
    this.data.forEach(async val => {
      await this.authenticationService.likeSong({
        song_aggregate_id: val,
        attitude_type: this.attitude
      });
    });
  }
}
