import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { AuthenticationService } from "../_services/authentication.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TranslationsService } from "../_services/translations.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private _snackBar: MatSnackBar,
    private authenticationService: AuthenticationService,
    private translationService: TranslationsService
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError(err => {
        if ([401, 403].indexOf(err.status) !== -1) {
          this.authenticationService.logout();
          location.reload(true);
        }
        const message = err.error.message;
        this.translationService.languageSelected.subscribe(val => {
          let snackBarOk: string = this.translationService.translations
            .errorInterceptor.snackBarOk[val];
          this._snackBar.open(message, snackBarOk);
        });
        return throwError(message);
      })
    );
  }
}
