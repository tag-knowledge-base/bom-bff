import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable, throwError } from "rxjs";
import { AuthService } from "angularx-social-login";
import { AuthenticationService } from "../_services/authentication.service";
import { map, catchError } from "rxjs/operators";
import { User } from "../_models/user";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.authenticationService
      .userAggregate()
      .then(val => {
        return true;
      })
      .catch(err => {
        this.authenticationService.logout();
        return false;
      });
  }
}
