import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatDialogModule, MatDialogActions } from "@angular/material/dialog";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatSelectModule } from "@angular/material/select";
import { MatIconModule } from "@angular/material/icon";
import {
  MatInputModule,
  MatCheckbox,
  MatCheckboxModule,
  MatTabsModule,
  MatRadioModule
} from "@angular/material";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatListModule } from "@angular/material/list";
import { MatChipsModule } from "@angular/material/chips";

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ToolbarComponent } from "./toolbar/toolbar.component";
import { JwtInterceptor } from "./_helpers/jwt.interceptor";
import { ErrorInterceptor } from "./_helpers/error.interceptor";
import { LocalizationswitchComponent } from "./localizationswitch/localizationswitch.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule } from "@angular/forms";
import { LogoutComponent } from "./logout/logout.component";
import { UrlInterceptor } from "./_helpers/url.interceptor";
import { HomeComponent } from "./home/home.component";
import { APP_BASE_HREF } from "@angular/common";
import { MatTableModule } from "@angular/material/table";
import { FooterComponent } from "./footer/footer.component";
import {
  ContributeComponent,
  ContributeDialogComponent,
  RateDialogComponent
} from "./contribute/contribute.component";
import { ProfileComponent } from "./profile/profile.component";

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(
      "421416174075-r1bvbkeoc5egg19jbvg912bgnc8cqb3b.apps.googleusercontent.com"
    )
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    LocalizationswitchComponent,
    LogoutComponent,
    HomeComponent,
    FooterComponent,
    ContributeComponent,
    ProfileComponent,
    ContributeDialogComponent,
    RateDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    MatButtonModule,
    HttpClientModule,
    MatCardModule,
    MatDialogModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatGridListModule,
    MatListModule,
    MatChipsModule,
    MatCheckboxModule,
    MatTabsModule,
    MatRadioModule
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: "/"
    },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: UrlInterceptor,
    //   multi: true
    // },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
