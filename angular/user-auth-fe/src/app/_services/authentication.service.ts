import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";

import { CreateUser, UserInDb, User, TagUser } from "../_models/user";
import {
  AuthService,
  GoogleLoginProvider,
  SocialUser
} from "angularx-social-login";
import { Role } from "../_models/role";
import { SongDto, CreateSongDto, LikeSongDto } from "../_models/tag";
import { UiLikedSongs } from "../profile/profile.component";

export interface BffException {
  localization_key: string;
  status_code: number;
  message: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  public currentUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(
    JSON.parse(localStorage.getItem("currentUser"))
  );

  constructor(private http: HttpClient, private googleAuth: AuthService) {}

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  async login() {
    const user: SocialUser | void = await this.googleAuth
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .catch(err => console.log(err));
    if (!user) {
      await this.logout();
      return;
    }
    const headers = new HttpHeaders({ "x-gmail-token": user.idToken });
    const userInDb: User | void = await this.http
      .get<User>("/api/platform/v1/user/gmail", {
        headers: headers
      })
      .toPromise()
      .catch(err => console.log(err));
    if (userInDb) {
      userInDb.user_token = user.idToken;
      userInDb.user_name = user.firstName + " " + user.lastName;
      localStorage.setItem("currentUser", JSON.stringify(userInDb));
      this.currentUserSubject.next(userInDb);
    } else {
      const userFromPost: User | void = await this.http
        .post<User>("/api/platform/v1/user/gmail", null, {
          headers: headers
        })
        .toPromise()
        .catch(err => console.log(err));
      if (!userFromPost) {
        await this.logout();
        return;
      }
      userFromPost.user_token = user.idToken;
      userFromPost.user_name = user.firstName + " " + user.lastName;
      localStorage.setItem("currentUser", JSON.stringify(userFromPost));
      this.currentUserSubject.next(userFromPost);
    }
    const registeredUser: TagUser | void = await this.http
      .get<TagUser>("/api/platform/v1/user/register/gmail", {
        headers: headers
      })
      .toPromise()
      .catch(err => console.log(err));
    if (registeredUser) {
      return;
    }
    const registeredUserPost: string | void = await this.http
      .post<string>(
        "/api/platform/v1/user/register/gmail",
        {},
        { headers: headers }
      )
      .toPromise()
      .catch(err => console.log(err));
    if (registeredUserPost) {
      const registeredUser: TagUser | void = await this.http
        .get<TagUser>("/api/platform/v1/user/register/gmail", {
          headers: headers
        })
        .toPromise()
        .catch(err => console.log(err));
      if (!registeredUser) {
        await this.logout();
      }
    }
    return;
  }

  async logout() {
    localStorage.removeItem("currentUser");
    this.currentUserSubject.next(null);
  }

  async userAggregate() {
    const headers = new HttpHeaders({
      "x-gmail-token": this.currentUserValue.user_token
    });
    return await this.http
      .get<TagUser>("/api/platform/v1/user/register/gmail", {
        headers: headers
      })
      .toPromise();
  }

  async deleteUser() {
    const headersDelete = new HttpHeaders({
      "x-gmail-token": this.currentUserValue.user_token
    });
    return await this.http
      .delete("/api/platform/v1/user/register/gmail", {
        headers: headersDelete
      })
      .toPromise();
  }

  async getSongs() {
    const headersGet = new HttpHeaders({
      "x-gmail-token": this.currentUserValue.user_token
    });
    let ret = await this.http
      .get("/api/platform/v1/song", {
        headers: headersGet,
        observe: "response"
      })
      .toPromise();
    if (ret.status !== 200) {
      throwError((<BffException>ret.body).message);
    }
    let res: SongDto[] = <SongDto[]>ret.body;
    return res;
  }

  async createSong(createSong: CreateSongDto) {
    const headers = new HttpHeaders({
      "x-gmail-token": this.currentUserValue.user_token
    });
    return await this.http
      .post("/api/platform/v1/song", createSong, {
        headers: headers
      })
      .toPromise();
  }

  async deleteSongsBulk(songs: string[]) {
    const headers = new HttpHeaders({
      "x-gmail-token": this.currentUserValue.user_token
    });
    return await this.http
      .post("/api/platform/v1/song/clear/bulk", songs, { headers: headers })
      .toPromise();
  }

  async likeSong(likeSong: LikeSongDto) {
    const headers = new HttpHeaders({
      "x-gmail-token": this.currentUserValue.user_token
    });
    return await this.http
      .post("/api/platform/v1/user/song/like", likeSong, { headers: headers })
      .toPromise();
  }
}
