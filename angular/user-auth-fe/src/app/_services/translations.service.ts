import { Injectable } from "@angular/core";
import { Translations } from "../_localization/translations";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TranslationsService {
  public translations = Translations();
  public languageSelected: BehaviorSubject<string> = new BehaviorSubject<
    string
  >(this.translations.availableLanguages[0]);
  // private languageSelected: string = this.translations.availableLanguages[0];

  constructor(private _snackBar: MatSnackBar) {}

  setLanguage(language: string) {
    this.languageSelected.next(language);
    // this.languageSelected = language;
  }
}
