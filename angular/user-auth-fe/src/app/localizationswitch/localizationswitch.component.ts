import { Component, OnInit, Output } from "@angular/core";
import { Translations } from "../_localization/translations";
import { TranslationsService } from "../_services/translations.service";
import { EventEmitter } from "@angular/core";
import { MatSelectChange } from "@angular/material/select";

@Component({
  selector: "app-localizationswitch",
  template: `
    <form class="text-field">
      <mat-form-field class="input-field">
        <mat-label>{{ formTitle }}</mat-label>
        <mat-select
          (selectionChange)="onSelectionChange($event)"
          [(ngModel)]="selectedLanguage"
          name="language"
          required
        >
          <mat-option *ngFor="let language of languages" [value]="language">
            {{ language }}
          </mat-option>
        </mat-select>
      </mat-form-field>
    </form>
  `,
  styles: [
    `
      .text-field {
        font-size: 12px;
      }
    `,
    `
      .input-field {
        width: 60px;
      }
    `
  ]
})
export class LocalizationswitchComponent implements OnInit {
  public selectedLanguage: string = Translations().availableLanguages[0];
  public languages: String[] = Translations().availableLanguages;
  public formTitle: string = "Language";

  constructor(private translationsService: TranslationsService) {}

  ngOnInit(): void {
    this.translationsService.languageSelected.subscribe(val => {
      this.formTitle = this.translationsService.translations.localizationSwitch.languageLabel[
        val
      ];
    });
  }

  onSelectionChange($event: MatSelectChange) {
    this.translationsService.setLanguage($event.value);
    this.updateLanguage();
  }

  updateLanguage() {
    this.translationsService.languageSelected.subscribe(val => {
      this.formTitle = this.translationsService.translations.localizationSwitch.languageLabel[
        val
      ];
    });
  }
}
