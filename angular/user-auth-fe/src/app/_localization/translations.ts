const translations = {
  availableLanguages: ["en-US"],
  errors: {
    "userauthbff-servererror": { "en-US": "Internal Server Error" },
    "userauthbff-cannotverifygmailuser": {
      "en-US": "Cannot verify gmail user"
    },
    "userauthbff-useridcollision": {
      "en-US": "User Id exists in database already"
    },
    "userauthbff-usernotfound": {
      "en-US": "User not found."
    }
  },
  translationsService: {
    languagenotfound: { "en-US": "Language not available" },
    snackBarOk: { "en-US": "OK" }
  },
  errorInterceptor: {
    snackBarOk: { "en-US": "OK" }
  },
  localizationSwitch: {
    languageLabel: { "en-US": "Language" }
  },
  toolbar: {
    appName: { "en-US": "ONTOSOUND" }
  },
  logout: {
    logout: { "en-US": "Logout" },
    welcome: { "en-US": "Profile" },
    login: { "en-US": "Login" }
  }
};

export function Translations() {
  return translations;
}
