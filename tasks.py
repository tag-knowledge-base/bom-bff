""" tasks.py """


import os
import invoke


LOCAL_NEXUS_REPO = os.getenv(
    "LOCAL_NEXUS_REPO", "206.189.186.153:8081/repository/pypi-repo/simple"
)
DOCKER_REPO = os.getenv("DOCKER_REPOSITORY", "206.189.186.153:8083")
APP_VERSION = "1.0.0-rc.4"
APP_NAME = "user-auth-bff"


@invoke.task()
def publish(c):
    """ publish """
    user = os.getenv("PYPI_USERNAME")
    password = os.getenv("PYPI_PASSWORD")
    c.run(f"docker login -u {user} -p {password} {DOCKER_REPO}")
    c.run(f"docker push {DOCKER_REPO}/{APP_NAME}:{APP_VERSION}")


@invoke.task
def build_angular(c):
    """ build angular """
    c.run("rm -rf ./dist")
    c.run("cd ./angular/user-auth-fe && ng build --prod", echo=True)
    c.run("cp -a ./angular/user-auth-fe/dist/* ./dist")


@invoke.task()
def build(c):
    """ build docker """
    c.run("rm -rf ./dist")
    c.run(
        "docker build . "
        f"--build-arg APP_NAME={APP_NAME} "
        f"--build-arg APP_VERSION={APP_VERSION} "
        "--build-arg PYPI_USERNAME=$PYPI_USERNAME "
        "--build-arg PYPI_PASSWORD=$PYPI_PASSWORD "
        f"--build-arg PYPI_REPOSITORY={LOCAL_NEXUS_REPO} "
        "--build-arg BFF_DB_USER=$BFF_DB_USER "
        "--build-arg BFF_DB_PASSWORD=$BFF_DB_PASSWORD "
        "--build-arg BFF_DB_HOST=$BFF_DB_HOST "
        "--build-arg BFF_ALEMBIC_SCRIPT_LOCATION=userauthbff "
        f"-t {DOCKER_REPO}/{APP_NAME}:{APP_VERSION}"
    )


@invoke.task(pre=[build_angular])
def run(c):
    """ run app """
    c.run("uvicorn --port 8000 --host localhost src.userauthbff.main:APP --reload")
