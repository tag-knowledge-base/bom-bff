ARG PYPI_USERNAME
ARG PYPI_PASSWORD
ARG PYPI_REPOSITORY
ARG BFF_DB_USER
ARG BFF_DB_PASSWORD
ARG BFF_DB_HOST

FROM node as angular
COPY ./angular/user-auth-fe ./angular
WORKDIR ./angular
RUN npm update && \
	npm install -g -N @angular/cli && \
	npm install && \
	ng build --prod

FROM python:3.8-slim-buster
ARG PYPI_USERNAME
ARG PYPI_PASSWORD
ARG PYPI_REPOSITORY
ARG BFF_DB_USER
ARG BFF_DB_PASSWORD
ARG BFF_DB_HOST
ENV PYPI_USERNAME=$PYPI_USERNAME
ENV PYPI_PASSWORD=$PYPI_PASSWORD
ENV PYPI_REPOSITORY=$PYPI_REPOSITORY
WORKDIR /app
COPY ./src ./src
COPY --from=angular ./angular/dist/user-auth-fe ./dist
COPY Pipfile* ./
RUN set -e && ls && \
	apt-get update && \
	apt-get install -y --no-install-recommends build-essential gcc && \
	pip install pipenv && \
	pipenv install --deploy --system --ignore-pipfile
ENV CONFIG_DIR=./userauthbff
ENV BFF_DB_USER=$BFF_DB_USER
ENV BFF_DB_PASSWORD=$BFF_DB_PASSWORD
ENV BFF_DB_HOST=$BFF_DB_HOST

ENTRYPOINT ["uvicorn", "--port=8000", "--host=0.0.0.0", "src.userauthbff.main:APP"]
